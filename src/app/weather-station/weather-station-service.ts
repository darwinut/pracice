
import {Injectable} from '@angular/core';
import{Weather} from "./weather-station.component.ts"
@Injectable()
export class WeatherService {

  weatherData:Weather[]=[];

  getWeatherData(): Array<Weather> {
    var listdata=this.lists;
    for(var i = 0; i < listdata.length; i++) {


      this.weatherData.push(new Weather(listdata[i].id,listdata[i].name,listdata[i].unit,listdata[i].value));



    }
return   this.weatherData;

  }


lists = [

  {
    "id": "temp",
    "name": "Temperatuur",
    "unit": "&deg;C",
    "value": 7.95076923077
  },
  {
    "id": "humid",
    "name": "Niiskus",
    "unit": "%",
    "value": 73.1630769231
  },
  {
    "id": "baro",
    "name": "&Otilde;hur&otilde;hk",
    "unit": "hPa",
    "value": 1032.556
  },
  {
    "id": "wind_dir",
    "name": "Tuule suund",
    "unit": "&deg",
    "value": 55.1917958326993
  },
  {
    "id": "wind_len",
    "name": "Tuule kiirus",
    "unit": "m/s",
    "value": 8.38207597346863
  },
  {
    "id": "lux",
    "name": "Valgustatus",
    "unit": "lx",
    "value": "-"
  },
  {
    "id": "sole",
    "name": "Kiirgusvoog",
    "unit": "W/m<sup>2</sup>",
    "value": 0
  },
  {
    "id": "gamma",
    "name": "Radioaktiivsus",
    "unit": "&micro;Sv/h",
    "value": "-"
  },
  {
    "id": "precip",
    "name": "Sademed",
    "unit": "mm/s",
    "value": 0
  }

];

}

