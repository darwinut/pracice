import { Component,OnInit } from '@angular/core';
import {WeatherService} from "./weather-station-service";
export class Weather  {

   public _id: string;
  public _name: string;
  public _unit: string;
  public _value:any;
  constructor( id: string,  name: string,  unit: string,  value:any){
    this._id=id;
    this._name=name;
    this._unit=unit;
    this._value=value;
  }


    get id():string {
      return this._id;
    }

  set id(value:string){
      this._id=value;
      }

  get name():string{
      return this._name;
      }

  set name(value:string){
      this._name=value;
      }

  get unit():string{
      return this._unit;
      }

  set unit(value:string){
      this._unit=value;
      }

  get value():any{
      return this._value;
      }

  set value(value:any){
      this._value=value;
      }
}

@Component({
    templateUrl: './weather-station.html',
    styleUrls: ['./weather-station.css']
})
export class WeatherStationComponent implements OnInit {
	public status = "Work in progress";
  public weathardata:Weather[];
  constructor(private weatherStationService: WeatherService){
   this.weatherStationService=weatherStationService;
  };
  ngOnInit(){
    this.populate();
  };
  populate(){
    console.log(this.weatherStationService.getWeatherData());
    this.weathardata = this.weatherStationService.getWeatherData();

  };

}
